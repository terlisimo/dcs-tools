# DCS TOOLS #

---

### missiondiff.lua ###

Usage: missiondiff.lua <old mission file> <new mission file>

Finds differences between two serialized lua tables, intended for use with "mission" file contained in a DCS .miz file. Writes to stdout.

---

### missionpatch.lua ###

Usage: missionpatch.lua <mission file to patch> <patch file>

Merges output from missiondiff.lua into an existing mission file. Writes to stdout.

---

### Intended workflow: ###

1) create diff

* open mission file, edit, save
* extract file "mission" from both old and new .miz file. Give them different names, for example "oldmissionfile" and "newmissionfile".
* run "lua missiondiff.lua oldmissionfile newmissionfile > mission.diff"
* edit the mission.diff with a text editor and remove unwanted changes (such as map position and zoom level).

2) apply diff

* extract file "mission" from target .miz file
* run "lua missionpatch.lua mission mission.diff > mission.new"
* for validation, check diff of old vs new file with "lua missiondiff.lua mission mission.new"
* rename mission.new to "mission" and push it back into the .miz file

It is normal for resulting file to be significantly smaller than the original, since Mission Editor adds comments to its mission file, and the patching process gets rid of those.

Make sure you run the missiondiff.lua against the patched file and verify output (should be equal to the diff file).

---

### Known issues ###

1) Sensitivity to small changes in numerical values

Seems like the Mission Editor has a lossy conversion between int/float at one point.

You may find that there's a ton of numerical values (usually map coords) changed after a simple edit.

The missiondiff.lua attempts to skip over these small changes by using this ugly hack:

if v - nv > 0.0000000001 or nv - v > 0.0000000001 then

If you run into issues where diff is detecting this noise, or not detecting valid changes, try removing/adding a zero from that comparison.


2) Limited scope

These two scripts currently work only on the "mission" file. The .miz archive contains other files as well, which could be changed by your edits, such as "warehouse", "options" etc. 

Theoretically, same process could be used to diff all the other files, since they also seem to be lua code. Currently the scripts are hardcoded to only edit the "mission" file, which should work fine for small changes.

---

### WARNING!!! ###

Both missiondiff.lua and missionpatch.lua **load and execute** the arguments, which are expected to be valid lua code.

In other words, you're executing lua code in your current environment. If you're concerned about security, run this in a VM.

---

