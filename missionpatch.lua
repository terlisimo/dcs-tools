if arg[1] == nil or arg[2] == nil then
    io.write ("Usage: lua missionpatch.lua <mission file to patch> <patch file> \n")
    io.write ("\n Result is written to stdout!")
    return
end

f = loadfile(arg[1])
if f == nil then
    io.write("Error loading " .. arg[1])
    return
else
    f()
end

f = loadfile(arg[2])
if f == nil then
    io.write("Error loading " .. arg[2])
    return
else
    f()
end

function serialize (o, indent, tablename)
  if tablename then
    io.write(tablename," = ")
  end
  indent = indent or 0
  
  is = string.rep(" ", indent)
  if type(o) == "number" then
    io.write(o)
  elseif type(o) == "string" then
    io.write(string.format("%q", o))
  elseif type(o) == "boolean" then
    if o then
      io.write("true")
    else
      io.write("false")
    end
  elseif type(o) == "table" then
    io.write("\n" .. string.rep(" ", indent) .. "{\n")
    for k,v in pairs(o) do
      if type(k) == "number" then
        io.write(string.rep(" ", indent + 4), "[", k, "]", " = ")
      else
        io.write(string.rep(" ", indent + 4), "[\"", k, "\"]", " = ")
      end
      serialize(v, indent + 4)
      io.write(",\n")
    end
    io.write(string.rep(" ", indent) .. "}")
  else
    error("cannot serialize a " .. type(o))
  end
end


serialize(mission, nil , "mission")




