if arg[1] == nil or arg[2] == nil then
    io.write ("Usage: lua missiondiff.lua <old mission file> <new mission file>")
    return
end

f = loadfile(arg[1])
if f == nil then
    io.write("Error loading " .. arg[1])
    return
else
    f()
    m1 = mission
end

f = loadfile(arg[2])
if f == nil then
    io.write("Error loading " .. arg[2])
    return
else
    f()
    m2 = mission
end


function copynew(oldtable, newtable, tablename)
  local t1 = oldtable
  local t2 = newtable
  local k, v
  for k,v in pairs(t1) do
    local subtable = tablename
    if type(k) == "string" then
      subtable = subtable .. "[\"" .. k .. "\"]"
    else
      subtable = subtable .. "[" .. k .. "]"
    end
    if type(v) == "table" then
      copynew(t1[k],t2[k],subtable)
    else
      local nv = t2[k]
      if nv ~= nil then
        if v == nv then
          t1[k] = nil
          t2[k] = nil
        else          
          if type(nv) == "string" then
            io.write(subtable,"=")
            nv = string.gsub(nv, "\"", "\\\"")
            io.write(nv, "\"\n")
          elseif type(nv) == "boolean" then
            io.write(subtable,"=")
            if v then
              io.write("true\n")
            else
              io.write("false\n")
              end
          elseif type(nv) == "number" then
            if v - nv > 0.0000000001 or nv - v > 0.0000000001 then
              io.write(subtable,"=",nv,"\n")
            end
          end
          t1[k] = nil
          t2[k] = nil
        end
      else
        io.write(subtable,"=nil\n")
      end
    end
  end
  if tablename == "mission" then
    dbg = 1
  end
end

function pruneold(oldtable, newtable, tablename)
  local t1 = oldtable
  local t2 = newtable
  local k, v
  for k,v in pairs(t2) do
    local isempty = true
    local subtable = tablename .. "[\"" .. k .. "\"]"
    if type(v) == "table" then
      pruneold(t1[k],t2[k],subtable)
    else
      isempty = false
      local nv = t1[k]
      if nv == nil then
        io.write(subtable,"=")
        if type(v) == "string" then
          io.write("\"", v, "\"\n")
        elseif type(v) == "boolean" then
          if v then
            io.write("true\n")
          else
            io.write("false\n")
            end
        elseif type(v) == "number" then
          io.write(v,"\n")
        end
        t1[k] = nil
        t2[k] = nil
      else
        io.write(subtable,"=nil\n")
      end
    end
    if isempty then
      t1[k] = nil
      t2[k] = nil
    end
  end
  if tablename == "mission" then
    dbg = 1
  end
end

copynew(m1, m2, "mission")
pruneold(m1, m2, "mission")

